# Slip39

Elixir implementation of the [SLIP-0039 : Shamir's Secret-Sharing for Mnemonic Codes](https://github.com/satoshilabs/slips/blob/master/slip-0039.md).

This implementation is largely inspired by the [reference implementation](http://github.com/trezor/python-shamir-mnemonic/).

This work is very in progress. Any contribution is welcome.

## Installation

```elixir
def deps do
  [
    {:slip39, "~> 0.1.0"}
  ]
end
```

## Sample usage

```elixir
iex(1)> Mix.install([ {:slip39, "~> 0.1.0"}])
:ok
iex(2)> Slip39.generate_mnemonics!(2,5,"My super hidden secret")
[
  ["acid smug academic acid argue forbid lobe spill elder mayor submit huge gray vintage fiscal fortune total mailman inherit texture exhaust float fake flea galaxy",
   "acid smug academic agency artwork medical born clogs rescue cover moisture fiscal floral traffic moisture rescue clogs suitable inside emphasis overall shrimp hormone webcam dominant",
   "acid smug academic always arena vexed rocky devote parcel ending enlarge busy aquatic award secret often glance corner income platform furl kidney actress nylon estimate",
   "acid smug academic aquatic apart chest dance secret flip temple activity clogs deal believe adequate justice painting jerky hush auction medal unfold duckling dragon database",
   "acid smug academic axis aunt else victim jewelry detect cards tidy temple bolt location human realize type lying join husky syndrome fitness cover belong quick"]
]

iex(3)> mnemonics = ["acid smug academic always arena vexed rocky devote parcel ending enlarge busy aquatic award secret often glance corner income platform furl kidney actress nylon estimate", 
...(3)> "acid smug academic axis aunt else victim jewelry detect cards tidy temple bolt location human realize type lying join husky syndrome fitness cover belong quick"]
iex(4)> mnemonics |> Slip39.combine_mnemonics()
"My super hidden secret"

```

## DISCLAIMER

This module is supplied as is. Since it has not been audited and can
contains bugs, it should not be used to manipulate valuable secrets. If you decide to
ignore this warning, use basic OPSEC common sense and, at the very least, use a
computer booted on a USB key, then taken offline during calls to the functions of this
module. In addition, each mnemonic generated must never be stored in an electronic format
and must be carefully double-checked. In any case, its/their authors cannot be held responsible
for any loss of data or its financial consequences resulting from the direct or indirect
use of this module.

## How to contribute

I'm a beginner/mid-level Elixir developer, so please feel free to help improving this module. Here are some improvement ideas : 

* Some code parts are too much copy/pasted from reference implementation and should be refactored use more Elixir specific
syntax and constructs
* I'm raising a lot of exception : I thing this gives simplier code. I'm not yet sure if it respects Elixir coding guidelines and we should switch to non raising function pattern (ie, returning `{:ok, value}` or `{:error, reason}`). I'm afraid this would lead to more complicated/unreadable code for not much benefits. Any input (for example showing me and example library) welcome.