defmodule Slip39.Rs1024 do
  alias Slip39.Utils
  alias Slip39.Utils.Range
  alias Slip39.Constants
  import Bitwise

  @gen {
    0xE0E040,
    0x1C1C080,
    0x3838100,
    0x7070200,
    0xE0E0009,
    0x1C0C2412,
    0x38086C24,
    0x3090FC48,
    0x21B1F890,
    0x3F3F120
  }

  defp polymod(values) when is_list(values) do
    values
    |> Enum.reduce(1, fn v, chk ->
      b = chk >>> 20
      chk = bxor((chk &&& 0xFFFFF) <<< 10, v)

      Range.range(10)
      |> Enum.reduce(chk, fn i, chk ->
        bxor(chk, if((b >>> i &&& 1) !== 0, do: elem(@gen, i), else: 0))
      end)
    end)
  end

  @spec create_checksum(bitstring()) :: bitstring()
  @doc """
  Create a checksum (see spec). Data passed as argument should
  be a bitstring of multiple of radix_size, that is 10

      iex(1)> Slip39.Rs1024.create_checksum("Long life to Elixir!")
      <<183, 154, 93, 32::size(6)>>
  """
  def create_checksum(data) do
    # The checksum is done by looping on each mmemonic word
    # so we must chunk our data into RADIX_BITS bits chunk
    customization_string = Constants.get!(:SALT_STRING)
    radix_bits = Constants.get!(:RADIX_BITS)
    checksum_length_word = Constants.get!(:CHECKSUM_LENGTH_WORDS)

    ten_bit_integer_list =
      data
      |> Utils.chunk_bits(radix_bits)
      |> Enum.map(fn item ->
        <<n::size(radix_bits)>> = item
        n
      end)

    values =
      String.to_charlist(customization_string) ++
        ten_bit_integer_list ++ for _ <- Range.range(checksum_length_word), do: 0

    polymod = polymod(values) |> bxor(1)

    checksum_word_list =
      for i <- Range.range(checksum_length_word) |> Range.reverse(),
          do: polymod >>> (10 * i) &&& 1023

    # Finally, we concat this into 10 bits words bitstring
    checksum_word_list
    |> Enum.reduce(<<>>, fn word, acc ->
      <<acc::bitstring, word::size(radix_bits)>>
    end)
  end

  @doc """
  Verify that the given data, included at the end
  of the data, is valid (see spec).

      iex(1)> checksum = Slip39.Rs1024.create_checksum("Long life to Elixir!")
      <<183, 154, 93, 32::size(6)>>
      iex(2)> Slip39.Rs1024.is_checksum_valid?(<<"Long life to Elixir!"::bitstring, checksum::bitstring>>)
      true
      iex(3)>  Slip39.Rs1024.is_checksum_valid?(<<"Long life to Elixir?"::bitstring, checksum::bitstring>>)
      false
  """
  @spec is_checksum_valid?(bitstring()) :: boolean()
  def is_checksum_valid?(data_and_checksum) do
    # Following the spec, we want to create a list of integers
    # for each mnemonic word, so we chunk back our bitstring
    # using RADIX_BITS size.

    customization_string = Constants.get!(:SALT_STRING)
    radix_bits = Constants.get!(:RADIX_BITS)

    ten_bit_integer_list =
      data_and_checksum
      |> Utils.chunk_bits(radix_bits)
      |> Enum.map(fn item ->
        <<n::size(radix_bits)>> = item
        n
      end)

    (String.to_charlist(customization_string) ++ ten_bit_integer_list) |> polymod() == 1
  end
end
