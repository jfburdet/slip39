defmodule Slip39.Utils do
  alias Slip39.Constants

  @doc """
  Chunk the given bitstring into an array of bitstring
  of n length.

      iex(1)> Slip39.Utils.chunk_bits(<<1::1, 1::1, 1::1, 1::1>>,2)
      [<<3::size(2)>>, <<3::size(2)>>]
  """
  @spec chunk_bits(bitstring(), any()) :: [bitstring()]
  def chunk_bits(bitstring, n) do
    for <<chunk::size(n) <- bitstring>>, do: <<chunk::size(n)>>
  end

  @doc """
  When an enum needs to be accessed by its index, this function gives
  an efficient way and cleaner syntax to do it.

      iex(1)> my_data = ["zero", "one", "two"]
      ["zero", "one", "two"]
      iex(2)> my_indexed_data = Slip39.Utils.enum_to_indexed_map(my_data)
      %{0 => "zero", 1 => "one", 2 => "two"}
      iex(3)> my_indexed_data[2]
      "two"
  """
  def enum_to_indexed_map(enum) do
    enum
    |> Enum.with_index()
    |> Enum.map(fn {item, index} ->
      {index, item}
    end)
    |> Enum.into(%{})
  end

  def is_string_only_ascii(string) do
    string
    |> String.to_charlist()
    |> Enum.all?(&(&1 in 32..126))
  end

  @doc """
  Round up bit count to whole bytes.

      iex(1)> Slip39.Utils.bits_to_bytes(9)
      2
  """
  def bits_to_bytes(n) do
    round_bits(n, 8)
  end

  defp round_bits(n, radix_bits) do
    div(n + radix_bits - 1, radix_bits)
  end

  @doc """
  Round up an integer to the next multiple given a second argument

      iex(1)> Slip39.Utils.nearest_upper_multiple(41, 10)
      50
  """
  @spec nearest_upper_multiple(integer(), integer()) :: integer()
  def nearest_upper_multiple(n, multiple) do
    reminder = rem(n, multiple)

    n +
      if reminder == 0 do
        0
      else
        multiple - reminder
      end
  end

  @doc """
  Randomness source for the module.
  If deterministic testing is needed, one can configure the
  source for test MIX_ENV here in future revision.
  """
  @spec rnd_bytes(integer()) :: binary()
  def rnd_bytes(len) do
    :crypto.strong_rand_bytes(len)
  end

  @doc """
  Return a random share definition.
  This is mainly used for testing.
  """
  def random_share() do
    count = 2..Constants.get!(:MAX_SHARE_COUNT) |> Enum.random()
    threshold = 1..count |> Enum.random()

    {threshold, count}
  end
end
