defmodule Slip39.Utils.Range do
  @moduledoc """
  Contains helpers function to manipulate
  ranges in the same way as the reference libary
  writen in python.
  """

  def range(n) do
    range(0, n)
  end

  def range(a, b) do
    if b > a do
      a..(b - 1)
    else
      range_empty()
    end
  end

  defp range_empty() do
    1..0//1
  end

  def reverse(range) do
    range.last..range.first//range.step * -1
  end
end
