defmodule Slip39.Share.RawShare do
  defstruct [
    :x,
    :data
  ]
end
