defmodule Slip39.Share.CommonParameters do
  @moduledoc """
  Parameters that are common to all shares of a master secret.
  """
  defstruct [
    :identifier,
    :iteration_exponent,
    :group_index,
    :group_threshold,
    :group_count
  ]
end
