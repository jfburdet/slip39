defmodule Slip39.Share.GroupParameters do
  @moduledoc """
  Parameters that are common to all shares of a master secret, which belong to the same group.
  """

  defstruct [
    :identifier,
    :iteration_exponent,
    :group_index,
    :group_threshold,
    :group_count,
    :member_threshold
  ]
end
