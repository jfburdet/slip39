defmodule Slip39.Constants do
  alias Slip39.WordList
  alias Slip39.Shamir

  # module constants will be computed and assigned
  # at compile time.
  @constants %{
    :RADIX_BITS => 10,
    :word_list_map_by_string => WordList.word_list_map_by_string(),
    :word_list_map_by_idx => WordList.word_list_map_by_idx(),
    :SALT_STRING => "shamir",
    :CHECKSUM_SIZE => 30,
    :SECRET_INDEX => 255,
    :DIGEST_INDEX => 254,
    :DIGEST_LENGTH_BYTES => 4,
    :ID_LENGTH_BITS => 15,
    :ROUND_COUNT => 4,
    :BASE_ITERATION_COUNT => 10000,
    :MAX_SHARE_COUNT => 16,
    :CHECKSUM_LENGTH_WORDS => 3,
    :METADATA_LENGTH_BITS => 40 + 30,
    :MIN_MNEMONIC_LENGTH_BITS => 200,
    :GROUP_PREFIX_LENGTH_WORDS => 3,
    :EXP_TABLE => Shamir.compute_exp_log_tables() |> elem(0),
    :LOG_TABLE => Shamir.compute_exp_log_tables() |> elem(1)
  }

  def get!(key) do
    @constants |> Map.fetch!(key)
  end
end
