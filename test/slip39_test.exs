defmodule Slip39Test do
  use ExUnit.Case
  alias Slip39.Utils
  doctest Slip39, except: [generate_mnemonics!: 3, generate_mnemonics!: 5]

  test "combine two mnemonics" do
    secret =
      """
      focus walnut acrobat romp climate triumph likely switch display secret gravity bulge distance bishop unknown founder froth papa focus else
      focus walnut beard romp capacity blimp indicate beam retailer papa group insect society unfold oven voter fancy traveler program founder
      """
      |> String.split("\n", trim: true)
      |> Slip39.combine_mnemonics()

    assert secret ==
             <<73, 181, 216, 193, 168, 200, 111, 226, 177, 130, 238, 106, 244, 216, 147, 7>>
  end

  test "3 of 5 share for a 128 bits secret" do
    master_secret = Utils.rnd_bytes(16)

    assert Slip39.generate_mnemonics!(3, 5, master_secret)
           |> Enum.at(0)
           |> Enum.take_random(3)
           |> Slip39.combine_mnemonics() == master_secret
  end

  test "3 of 5 share for a 256 bits secret" do
    master_secret = Utils.rnd_bytes(32)

    assert Slip39.generate_mnemonics!(3, 5, master_secret)
           |> Enum.at(0)
           |> Enum.take_random(3)
           |> Slip39.combine_mnemonics() == master_secret
  end

  test "random of random share for a 128 bits secret" do
    master_secret = Utils.rnd_bytes(16)

    {threshold, count} = Utils.random_share()

    assert Slip39.generate_mnemonics!(threshold, count, master_secret)
           |> Enum.at(0)
           |> Enum.take_random(threshold)
           |> Slip39.combine_mnemonics() == master_secret
  end

  test "random of random share for a 256 bits secret" do
    master_secret = Utils.rnd_bytes(32)

    {threshold, count} = Utils.random_share()

    assert Slip39.generate_mnemonics!(threshold, count, master_secret)
           |> Enum.at(0)
           |> Enum.take_random(threshold)
           |> Slip39.combine_mnemonics() == master_secret
  end

  test "1000 times, random share def, random secret size" do
    # I had bug that showed sometime due to buggy
    # calculus and showing only on certains random
    # data.
    # To make things faster, we paralelize things.
    1..1000
    |> Task.async_stream(
      fn _ ->
        master_secret = Slip39.Utils.rnd_bytes((8..256 |> Enum.random()) * 2)
        {threshold, count} = Utils.random_share()

        assert Slip39.generate_mnemonics!(threshold, count, master_secret)
               |> Enum.at(0)
               |> Enum.take_random(threshold)
               |> Slip39.combine_mnemonics() == master_secret
      end,
      max_concurency: :erlang.system_info(:logical_processors_available),
      timeout: :infinity
    )
    # To start stream consumption, we 'Enum.map' it.
    |> Enum.map(& &1)
  end

  test "3 of 5 34 byte secret" do
    # Having secret of 34 byte lead to discover a bug in the code
    # because 34 byte lead to a 280 byte share value, and since
    # its byte length must be even, it lead to a 8 bits padding
    # which can cause the share value to be incorectly decoded and
    # prepending it with a zero byte.

    master_secret = Slip39.Utils.rnd_bytes(34)

    {threshold, count} = {3, 5}

    assert Slip39.generate_mnemonics!(threshold, count, master_secret)
           |> Enum.at(0)
           |> Enum.take_random(threshold)
           |> Slip39.combine_mnemonics() == master_secret
  end

  test "permuting two words gives crc failure" do
    master_secret = Slip39.Utils.rnd_bytes(32)
    {threshold, count} = {3, 5}

    subgroup =
      Slip39.generate_mnemonics!(threshold, count, master_secret)
      |> Enum.at(0)
      |> Enum.take_random(threshold)

    # let's take first mnemonic in the group and permute
    # two random words
    mnemonic = subgroup |> Enum.at(0) |> String.split(" ", trim: true)
    [first_idx | [second_idx]] = 0..((mnemonic |> length()) - 1) |> Enum.take_random(2)
    first_word = mnemonic |> Enum.at(first_idx)
    second_word = mnemonic |> Enum.at(second_idx)

    mnemonic =
      mnemonic
      |> List.replace_at(first_idx, second_word)
      |> List.replace_at(second_idx, first_word)

    subgroup = subgroup |> List.replace_at(0, mnemonic |> Enum.join(" "))

    assert_raise RuntimeError, "bad checksum", fn ->
      Slip39.combine_mnemonics(subgroup)
    end
  end

  test "too short secret" do
    assert_raise RuntimeError, "The length of the master secret must be at least 128 bits.", fn ->
      Slip39.generate_mnemonics!(3, 5, "too short")
    end
  end
end
