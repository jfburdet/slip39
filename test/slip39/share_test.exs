defmodule Slip39.ShareTest do
  use ExUnit.Case
  doctest Slip39.Share

  test "mnemonic to indices" do
    mnemonic =
      "ugly academic acrobat leader amount shadow discuss shame script airline switch editor skin human scholar brother email pulse public best headset vanish boundary scroll necklace submit round curly traveler fawn craft group aide"

    assert Slip39.Share.mnemonic_to_indices!(mnemonic) == [
             946,
             0,
             4,
             512,
             36,
             801,
             225,
             803,
             793,
             21,
             885,
             261,
             818,
             445,
             788,
             101,
             273,
             708,
             707,
             77,
             429,
             968,
             94,
             794,
             608,
             874,
             770,
             182,
             932,
             337,
             167,
             412,
             19
           ]
  end

  test "decode with success" do
    mnemonic =
      "pickup flea ceramic shaft document bulge alpha material estate random indicate wavy genius diet capital gums mouse glance index orange"

    assert Slip39.Share.decode!(mnemonic) ==
             %Slip39.Share{
               identifier: 21387,
               iteration_exponent: 0,
               group_index: 2,
               group_threshold: 2,
               group_count: 4,
               member_index: 2,
               member_threshold: 3,
               data: <<233, 26, 193, 216, 217, 43, 181, 157, 15, 145, 133, 54, 199, 134, 142, 85>>
             }
  end

  test "decode 280 bits share value" do
    # This test was added, because at first the code was buggy
    # with share value of 280 which is padded with height zero bits

    mnemonic =
      "priority lungs academic agree acid hanger usher fortune husky game ceiling thunder axis walnut squeeze wrap estate manager swimming ugly family losing depict overall juice liberty mason analysis coastal rebound preach detailed dough vegan vitamins"

    assert Slip39.Share.decode!(mnemonic) ==
             %Slip39.Share{
               identifier: 22257,
               iteration_exponent: 1,
               group_index: 0,
               group_threshold: 1,
               group_count: 1,
               member_index: 1,
               member_threshold: 3,
               data:
                 <<90, 127, 13, 108, 112, 151, 226, 11, 148, 16, 126, 13, 107, 247, 74, 226, 221,
                   207, 178, 82, 97, 179, 58, 122, 122, 32, 184, 212, 38, 39, 45, 202, 184, 213>>
             }
  end

  test "decode checksum error" do
    mnemonic =
      "ugly ugly acrobat leader amount shadow discuss shame script airline switch editor skin human scholar brother email pulse public best headset vanish boundary scroll necklace submit round curly traveler fawn craft group aide"

    assert_raise RuntimeError, "bad checksum", fn ->
      Slip39.Share.decode!(mnemonic)
    end
  end

  test "invalid word" do
    mnemonic =
      "ugly ugly acrobat laeder amount shadow discuss shame script airline switch editor skin human scholar brother email pulse public best headset vanish boundary scroll necklace submit round curly traveler fawn craft group aide"

    assert_raise RuntimeError, "Invalid words found : laeder", fn ->
      Slip39.Share.decode!(mnemonic)
    end
  end
end
