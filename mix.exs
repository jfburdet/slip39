defmodule Slip39.MixProject do
  use Mix.Project

  @source_url "https://gitlab.com/jfburdet/slip39"
  @version "0.1.1"

  def project do
    [
      app: :slip39,
      version: @version,
      elixir: "~> 1.15",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      description: description(),
      docs: docs(),
      package: package(),
      source_url: @source_url
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, ">= 0.0.0", only: :dev},
      {:plug_crypto, "~> 2.0"}
    ]
  end

  defp description() do
    "Elixir implementation of the SLIP-0039 : Shamir's Secret-Sharing for Mnemonic Codes."
  end

  defp docs do
    [
      extras: ["README.md"],
      main: "readme",
      source_ref: "v#{@version}",
      source_url: @source_url
    ]
  end

  defp package() do
    [
      name: "slip39",
      licenses: ["Apache-2.0"],
      links: %{
        "GitLab" => @source_url
      }
    ]
  end
end
